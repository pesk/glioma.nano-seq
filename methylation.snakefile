
rule callAll:
    input:
        expand("methylation/{sample}.tsv", sample=SAMPLES)

rule nanopolish_CpG_calling:
    input:
        fa="fasta/{sample}.fa",
        bam="bam_mapq/{sample}.bam",
        bai="bam_mapq/{sample}.bam.bai",
        ref=config["ref_genome_masked"]
    output:
        "methylation/{sample}.tsv"
    benchmark:
        "benchmarks/{sample}.CpGcall.benchmark.txt"
    threads:8
    shell:
        "~/tools/nanopolish-aaaf90b/nanopolish call-methylation -v --threads {threads} --reads {input.fa} --bam {input.bam} --genome {input.ref} > {output}"

rule pileup_CpG:
    input:
        "methylation/{sample}.tsv"
    output:
        "methylation/{sample}.pileup.txt"
    shell:
        "/export/data/applications/anaconda/2.4.0/bin/python ~/tools/nanopolish-0.6.0/scripts/calculate_methylation_frequency.py -c 2.5 -i {input} > {output}"

rule readCpGs:
    input:
        "methylation/{sample}.pileup.txt"
    output:
        "methylation/{sample}.RData"
    script:
        "scripts/readCpGs.R"

rule randomForest_GliomaSubtypes:
    input:
        "methylation/{sample}.RData"
    output:
        pdf="plots/{sample}-RF.pdf",
        votes="methylation/{sample}-votes.RData"
    script:
        "scripts/classifyMethylGlioma.R"

rule randomForest_PanCancer:
    input:
        meth="methylation/{sample}.RData",
        cn="igv/{sample}-500-0.05.bed",
        trainingset_meth="/lnec/SANSON/nanopore/trainingsets/methylation/pancan-450K-LUNG.h5",
        trainingset_cn="/lnec/SANSON/nanopore/trainingsets/CN/panTCGA_CN_bin.RData"
    output:
        pdf="plots/{sample}-pancan-RF-{modality}.pdf",
        votes="methylation/{sample}-pancan-votes-{modality}.RData",
        tmp=temp("tmp/{sample}-pancan-RF-{modality}.bed")
#    wildcard_constraints:
#        modality="CN|meth|both"
    benchmark:
        "benchmarks/{sample}.RF.pancan.{modality}.benchmark.txt"
    script:
        "scripts/classifyPanCancer.R"
