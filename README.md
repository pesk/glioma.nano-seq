## Same-day genomic and epigenomic diagnosis of brain tumors using realtime nanopore sequencing

This repository contains snakemake-based pipelines and scripts to recapitulate results in our cancer nanopore-sequencing manuscript.