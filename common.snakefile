
rule CNplots:
    input:
        expand("plots/{sample}-{binsize}-{alpha}-CNplot.pdf",sample=SAMPLES,binsize="500 1000".split(),alpha="0.05"),

rule CNtracks:
    input:
#        expand("igv/{sample}.chrom.cn.seg",sample=SAMPLES),
        expand("igv/{sample}.wig",sample=SAMPLES)

rule alignAll:
    input:
        expand("bam/{sample}.{ext}", sample=SAMPLES, ext="bam bam.bai".split())

rule RFplots:
    input:
        expand("plots/{sample}-RF.pdf",sample=SAMPLES),
        expand("plots/{sample}-pancan-RF-{modality}.pdf",sample=SAMPLES,modality="CN meth both".split())


rule BWA_align:
    input:
        fq="fasta/{sample}.fa",
        ref=config["ref_genome"]
    output:
        "bam/{sample}.bam"
    benchmark:
        "benchmarks/{sample}.align.benchmark.txt"
    threads: 12
    shell:
        "~/tools/bwa-0.7.12/bwa mem -x ont2d -t {threads} {input.ref} {input.fq} | ~/tools/samtools-1.3.1/samtools view -bS - | ~/tools/samtools-1.3.1/samtools sort - > {output} "

rule indexBAM:
    input:
        "{sample}.bam"
    output:
        "{sample}.bam.bai"
    shell:
        "~/tools/samtools-1.3.1/samtools index {input}"

rule filterMAPQ:
    input:
        "bam/{sample}.bam"
    output:
        "bam_mapq/{sample}.bam"
    params:
        minMAPQ="20"
    shell:
        "~/tools/samtools-1.3.1/samtools view -b -q {params.minMAPQ} {input} > {output}"

rule filterRepeats:
    input:
        bam="bam/{sample}_all.bam",
        bed="/lnec/SANSON/nanopore/pipeline/data/consensusBlacklist.bed"
    output:
        "bam/{sample}_norep.bam"
    shell:
        "~/tools/samtools-1.3.1/samtools view -b -L {input.bed} -U {output} {input.bam} > /dev/null"

rule coverage_wig:
    input:
        bam = "bam/{sample}.bam",
        bai = "bam/{sample}.bam.bai"
    output:
        "igv/{sample}.wig"
    threads: 1
    shell:
        "java -Xmx2G -Djava.net.useSystemProxies=true -Djava.awt.headless=true -jar ~/tools/IGVTools-2.3.90/igvtools.jar count -w 10000 {input.bam} {output} hg19"

rule CN_profile:
    input:
        bam="bam/{sample}.bam",
        bai="bam/{sample}.bam.bai",
        normal_bam="/lnec/SANSON/nanopore/PON/bam/FAF04090.bam"
    output:
        pdf="plots/{sample}-{binsize}-{alpha}-CNplot.pdf",
        rplot="plots/{sample}-CN-{binsize}-{alpha}.RData",
        bed="igv/{sample}-{binsize}-{alpha}.seg",
        bed1="igv/{sample}-{binsize}-{alpha}.bed"
    script:
        "scripts/createCNprofile.R"

rule CN_track:
    input:
        "igv/{sample}.bed"
    output:
        "igv/{sample}.chrom.cn.seg"
    shell:
        "Rscript scripts/makeCNtrack.R {input} {output}"
