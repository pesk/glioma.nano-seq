library(ggplot2)
#library(gridExtra)

### load 450K profiles for GlioTex and POLA series

load(snakemake@input[["POLA_450K"]])
colnames(POLA_450K) <- sub("HS_(\\w+)","X\\1T",colnames(POLA_450K))
POLA_450K$ID <- rownames(POLA_450K)

load(snakemake@input[["GlioTeX_450K"]])
colnames(gliotex450K) <- sub("X(\\w+).AVG_Beta","X\\1",colnames(gliotex450K))
gliotex450K$ID <- rownames(gliotex450K)

illumina <- merge(POLA_450K,gliotex450K,by="ID",all=T)

###

plot450KvsMinION <- function(sample,pdffile) {
  load(sample)
  tsample <- data.frame(ID = colnames(case), minION = as.factor(t(case)))
  
  sampleID <- sub(".*(\\d{4}T).*","\\1",sample)
  illuminaID <- paste("X",sampleID,sep="")

  m <- merge(illumina[,c("ID",illuminaID)], tsample, by="ID")

  p <- ggplot(m, aes_string(color = "minION", group = "minION", x = illuminaID)) + geom_freqpoly() + xlab("450K beta value") + labs(title = sampleID) + theme_classic() + scale_colour_brewer(type = "qual", palette = "Set1")
  p
  ggsave( pdffile )

  return(p)
}

plot450KvsMinION(snakemake@input[["CpGcalls"]],snakemake@output[[1]])

#plots <- lapply(snakemake@input, plot450KvsMinION)
#pdf("filename.pdf")
#grid.arrange(plots, ncol=3)
#dev.off()

