library(ggplot2)
library(gtools)

covPlots <- function(input, covSummaryPDF, covDetailPDF) {

  cov <- read.table(input,sep="\t",header=F)
  
  cov$V4 <- factor(cov$V4, levels=unique(mixedsort(as.character(cov$V4))))
  
  ggplot(cov, aes(x=V5, y=V6, group=V4)) +
    geom_area(aes(fill="red")) +
    facet_wrap(~ V4, scales = "free_x") +
    theme_classic() +
    guides(fill=FALSE) +
    xlab("position within amplicon (bp)") +
    ylab("depth (no. of reads)") +
    scale_y_continuous(trans = "log10") +
    theme(strip.background = element_blank() )
  ggsave(file = covDetailPDF)

  minCov <- aggregate(V6 ~ V4, cov, min)

  ggplot(minCov,aes(x=V4,y=V6)) +
    geom_bar(stat = "identity") +
    theme_classic() +
    theme(axis.text.x = element_text(angle = 90, hjust = 1)) +
    xlab("amplicon") +
    ylab("minimum read depth") +
    scale_y_continuous(trans = "log10")
  ggsave(file = covSummaryPDF)

}

covPlots(input = snakemake@input[[1]],
         covSummaryPDF = snakemake@output[["sumPDF"]],
         covDetailPDF = snakemake@output[["detailPDF"]])
