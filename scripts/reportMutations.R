library(VariantAnnotation)
library(xlsx)
library("BSgenome.Hsapiens.UCSC.hg19")

exportMut <- function(vcffile) {
  vcf <- readVcf(vcffile, genome = "hg19")

  context <- getSeq(Hsapiens,flank(rowRanges(vcf),5,both=T))
  hp <- grepl("A{3,}|T{3,}|G{3,}|C{3,}",context)

  tbl <- data.frame(mutation=rownames(info(vcf)), rowRanges(vcf), depth=info(vcf)$ADP, in_ExAC=info(vcf)$ExAC, hotspot=info(vcf)$CancerHotspot, homopolymer_context=hp, context)
  
  header <- colnames(tbl)
  header[2] <- "chromosome"
  header[3] <- "position"
  colnames(tbl) <- header
  
  tbl$ALT <- unlist(lapply(tbl$ALT,as.character))
  
  tbl <- tbl[,c(1:3,8:9,12:16)]
  
  write.xlsx(tbl, file=xlsxfile, sheetName = sub(".vcf","",basename(vcffile)), row.names = F, showNA = TRUE, append = T)
  
  return(context)
}

#xlsxfile <- snakemake@output[[1]]
#lapply(snakemake@input, exportMut)

xlsxfile <- "Z:/nanopore/amplicon.rev/reports/SupplTable3.xlsx"
contexts <- lapply(list.files("Z:/nanopore/amplicon.rev/variants_filtered/","*.vcf",full.names = T), exportMut)

contexts <- do.call(c, contexts)
names(contexts) <- 1:length(contexts) # give unique names by simple numbering
writeXStringSet(contexts, filepath = "test.fa")