library(ggplot2)

meanCov <- function(input) {
  cov <- read.table(input,sep="\t",header=F)
  agg <- aggregate(V6 ~ V4, cov, mean)
  return(mean(agg$V6))
}

#files = snakemake@input[[1]]
files <- list.files("Z:/nanopore/amplicon.rev/coverage/", full.names = T)

cov <- unlist(lapply(files, meanCov))
samples <- gsub("-amplicon|\\.txt","",basename(files))

all <- data.frame(sample = samples, meanCoverage = cov, multiplex = ifelse(grepl("4596|5539",samples),"FFPE",ifelse(grepl("3523|3427|2965|2483|2402|2197",samples),"single","multiplex")))

ggplot(all,aes(x = reorder(sample,meanCoverage), y = meanCoverage, fill = multiplex)) + 
  geom_bar(stat = "identity") + 
  theme_classic() +
  scale_fill_brewer(type="qual",palette = "Set1") +
  scale_y_log10(breaks=c(10,100,1000,10000)) +
  xlab("sample") +
  ylab("mean read depth (no. of reads)") +
  coord_flip()

save(all, file="meanCov.RData")