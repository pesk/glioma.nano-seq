library(QDNAseq)

bin.size = 1000

bins <- getBinAnnotations(binSize = as.numeric(bin.size))

#bins@data$gc <- 50
#bins@data$mappability <- 100

pon <- binReadCounts(bins, path="/lnec/SANSON/nanopore/PON/bam/")

pon <- applyFilters(pon, residual=FALSE, blacklist=FALSE, mappability=FALSE, bases=FALSE)

bins$residual <- iterateResiduals(pon)

save(bins, file = "/lnec/SANSON/nanopore/PON/bins_hg19_1000K.RData")