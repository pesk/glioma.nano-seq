stats <- read.table("/GBM/nano/batch1/flagstats/read_stats.txt", sep="\t")

stats$sample <- sub(".*/(.+)_h.*","\\1",stats$V1)
stats$time <- sub(".*_h(\\d+)\\.bam.*","\\1",stats$V1)

require(data.table)

stats <- data.table(subset(stats, grepl("_h",V1)))
stats <- stats[order(stats$sample,stats$time),]
stats <- data.frame(stats[, cumReads := cumsum(V2), by=sample])

#stats$time <- stats$time + 1

library(ggplot2)

pdf("plots/reads_by_time.pdf")
ggplot(stats, aes(x=time, y=cumReads, group=sample, colour=sample)) + geom_line() + xlab("time (hours)") + ylab("mapped reads")
dev.off()
