calcDuration <- function(x) {
  load(x) 
  return( max(startTimes$times) / 4000 / 60 )
}

times <- unlist(lapply(snakemake@input[[1]],calcDuration))

tbl <- data.frame(sample = snakemake@input[[1]], runtime = times)

write.table(tbl, file=snakemake@output[[1]], sep="\t", row.names = F, col.names = T)
