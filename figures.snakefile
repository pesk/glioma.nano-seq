GLIOMAS="8137T 2965T 2922T 2483T 2197T 7455T 3427T".split()
METASTASIS="8356T 8357T 8358T 8359T".split()

rule GC_plots:
    input:
        expand("figures/{sample}_GC.pdf", sample=SAMPLES)

rule nanopore_vs_450K_plots:
    input:
        expand("figures/{sample}_vs450K.pdf", sample=SAMPLES)

rule nanopore_vs_450K_plot:
    input:
        CpGcalls="methylation/{sample}.RData",
	POLA_450K="/lnec/SANSON/nanopore/pipeline/data/POLA_450K.RData",
        GlioTeX_450K="/lnec/SANSON/nanopore/pipeline/data/GlioTeX_450K.RData"
    output:
        "figures/{sample}_vs450K.pdf"
    script:
        "scripts/plot450K_vs_nanoWGS.R"


rule methyl_class_figure:
    input:
        expand("methylation/{sample}-votes.RData", sample=GLIOMAS)
    output:
        "figures/methyl_class_barplots.pdf"
    script:
        "scripts/classification_barplots.R"

rule pancan_class_figure:
    input:
        expand("methylation/{sample}-pancan-votes-{{modality}}.RData", sample=GLIOMAS)
    output:
        "figures/pancan_{modality}_class_barplots.pdf"
    script:
        "scripts/classification_barplots.R"


rule global_methylation_figure:
    input:
        expand("methylation/{sample}.RData", sample=SAMPLES)
    output:
        "figures/global_methylation_barplots.pdf"
    script:
        "scripts/global_methylation_barplots.R"


rule hg19_GC_dist:
    input:
        ref=config["ref_genome"]
    output:
        chopped="{params.prefix}.fa",
        subsampled="intermediate/hg19_1K_subsample.fa"
    params:
        prefix="intermediate/hg19_1K"
    shell:
        "faSplit size {input} 1000 {params.prefix} -oneFile ; "
        "fasta-subsample {output.chopped} 20000 > {output.subsampled}"

rule make_GC_plot:
    input:
        BAM="bam_mapq/{sample}.bam",
        ref_chopped="intermediate/hg19_1K_subsample.fa"
    output:
        GC_plot="figures/{sample}_GC.pdf",
	readlength_plot="figures/{sample}_readlength.pdf"
    script:
        "scripts/GC_histogram.R"

rule noReads:
    input:
        expand("fasta/{sample}.fa",sample=SAMPLES)
    output:
        "QC/noReads.txt"
    run:
        for f in input:
            shell("cat {f} | grep '>' | wc -l | echo \"{f} $(cat -)\" >> {output} || true")

rule make_yield_plot:
    input:
        stats="QC/noReads.txt",
        runinfo="/lnec/SANSON/nanopore/pipeline/data/runs.txt"
    output:
        "figures/read_yields.pdf"
    script:
        "scripts/read_yield_plot.R"


rule calc_coverage:
    input:
        bam="bam/{sample}.bam",
        bai="bam/{sample}.bam.bai",
        chr_sizes=config["chr_sizes"]
    output:
        "stats/{sample}.coverage.tsv"
    shell:
        "bedtools genomecov -ibam {input.bam} -g {input.chr_sizes} > {output}"

rule make_coverage_table:
    input:
        expand("stats/{sample}.coverage.tsv", sample=SAMPLES)
    output:
        "tables/meanMappedReadDepth.txt"
    script:
        "scripts/makeCoverageTable.R"

rule trainingSetHeatmap:
    input:
        trainingset_meth="/lnec/SANSON/nanopore/trainingsets/methylation/pancan-450K.h5",
        trainingset_cn="/lnec/SANSON/nanopore/trainingsets/CN/panTCGA_CN_bin.RData"
    output:
        "figures/fig3A_trainingSetHeatmap.pdf"
    script:
        "scripts/trainingSetHeatmap.R"

rule rename_CN_plot:
    input:
        CN="plots/{sample}-1000-0.05-CNplot.pdf"
    output:
        CN="plots/{sample}-final-CN.pdf"
    shell:
        "cp {input} {output}"

rule PDFreport_WGS:
    input:
        CN="plots/{sample}-final-CN.pdf",
        RF="plots/{sample}-pancan-RF-meth.pdf",
        GC="figures/{sample}_GC.pdf",
        RL="figures/{sample}_readlength.pdf"
    output:
        "figures/{sample}_WGS_report.pdf"
    script:
        "scripts/makePDFreport_WGS.R"


rule allFigures:
    input:
        rules.GC_plots.output,
        rules.methyl_class_figure.output,
        rules.global_methylation_figure.output,
#        rules.make_yield_plot.output,
        rules.make_coverage_table.output,
        rules.trainingSetHeatmap.output,
        expand("figures/{sample}_WGS_report.pdf",sample=SAMPLES),
        expand("figures/pancan_{modality}_class_barplots.pdf",modality="CN meth both".split())
