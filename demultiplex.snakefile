
DEMUX=config["DEMUX"]
BARCODES=config["BARCODES"]
NONBARCODED=config["NONBARCODED"]

rule getfasta:
    input:
        expand("fasta/{sample}.fa", sample=SAMPLES)


rule basecall_nobarcode:
    input:
        config["FAST5_basedir"] + "{sample}/"
    output:
        "nobarcode/{sample}/"
    wildcard_constraints:
        sample="|".join(config["NONBARCODED"])
    threads: 12
    benchmark:
        "benchmarks/{sample}.basecall.benchmark.txt"
    shell:
        "~/.conda/envs/nanopy3/bin/read_fast5_basecaller.py -t {threads} -f FLO-MIN106 -k SQK-RAD002 -r -i {input} -s {output} -o fastq,fast5"

rule basecall_demultiplex:
    input:
        config["FAST5_basedir"] + "{run}/"
    output:
        "demux/{run}/"
    threads: 12
    benchmark:
        "benchmarks/{run}.demux.benchmark.txt"
    shell:
        "~/.conda/envs/nanopy3/bin/read_fast5_basecaller.py -t {threads} -f FLO-MIN106 -k SQK-RBK001 -r -i {input} -s {output} -o fastq,fast5"


rule FAST5_to_FASTA_nobarcode_R9:
    input:
        config["FAST5_basedir"] + "{sample}/"
    output:
        passfa=temp("tmp/{sample}_pass.fa"),
        failfa=temp("tmp/{sample}_fail.fa"),
        fa="fasta/{sample}.fa"
    threads: 2
    wildcard_constraints:
        sample="|".join(config["NONBARCODED_R9"])
    benchmark:
        "benchmarks/{sample}.extract.benchmark.txt"
    shell:
        "~/tools/nanopolish-revreadgroups/nanopolish extract -t template -r {input}/pass/downloads/ > {output.passfa} ; "
        "~/tools/nanopolish-revreadgroups/nanopolish extract -t template -r {input}/fail/downloads/ > {output.failfa} ; "
        "cat {output.passfa} {output.failfa} > {output.fa}"

rule FAST5_to_FASTA_nobarcode:
    input:
        "nobarcode/{sample}/"
    output:
        "fasta/{sample}.fa"
    threads: 2
    wildcard_constraints:
        sample="|".join(config["NONBARCODED"])
    benchmark:
        "benchmarks/{sample}.extract.benchmark.txt"
    shell:
        "~/tools/nanopolish-revreadgroups/nanopolish extract -t template -r {input}/workspace/ > {output}"

rule FAST5_to_FASTA_barcoded:
    input:
        demux=lambda wildcards : "demux/" + DEMUX[wildcards.sample] + "/"
    output:
        "fasta/{sample}.fa"
    params:
        run=lambda wildcards : DEMUX[wildcards.sample],
        barcode=lambda wildcards : BARCODES[wildcards.sample]
    wildcard_constraints:
        sample="|".join(DEMUX.keys())
    threads: 2
    benchmark:
        "benchmarks/{sample}.extract.benchmark.txt"
    shell:
        "~/tools/nanopolish-revreadgroups/nanopolish extract -t template -r {input}/workspace/{params.barcode}/ > {output}"
