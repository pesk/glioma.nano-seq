
WES="2197T 3427T 3523T 2402T 2965T".split()

rule vcfs:
    input:
        expand("variants_filtered/{sample}_varscan.vcf",sample=SAMPLES)

rule allamplicon:
    input:
        expand("variants_filtered/{sample}_varscan.vcf",sample=SAMPLES),
        expand("plots/{sample}_cov_detail.pdf",sample=SAMPLES),
        expand("reports/{sample}_concordance.txt",sample=WES),
        expand("plots/{sample}_reads_over_time.pdf",sample=SAMPLES),
        #"reports/variants_filtered.xlsx",
        "reports/ampliconRuntimes.txt"

rule event_align:
    input:
        ref = config["ref_genome_masked"],
        fa = "fasta/{sample}.fa",
        bam = "bam_mapq/{sample}.bam",
        bai = "bam_mapq/{sample}.bam.bai"
    output:
        "polished/{sample}.bam"
    threads: 8
    shell:
        "nanopolish eventalign --verbose -t {threads} --sam -r {input.fa} -b {input.bam} -g {input.ref} --models nanopolish_models.fofn | samtools view -Sb - | samtools sort - > {output}"

rule call_variants:
    input:
        ref = config["ref_genome_masked"],
        fa = "fasta/{sample}.fa",
        bam = "bam_mapq/{sample}.bam",
        bai = "bam_mapq/{sample}.bam.bai",
        evtbam = "polished/{sample}.bam",
        evtbai = "polished/{sample}.bam.bai"
    params:
        minMAF = 0.2
    threads: 8
    output:
        "variants/{sample}.vcfx"
    shell:
        "nanopolish variants -m {params.minMAF} -w chr2:209098953-209121806 --verbose -t {threads} --reads {input.fa} --bam {input.bam} --event-bam {input.evtbam} --genome {input.ref} --models-fofn nanopolish_models.fofn --snps --progress > {output}"


rule call_variants_varscan:
    input:
        ref = config["ref_genome"],
        bam = "bam_mapq/{sample}.bam",
        bai = "bam_mapq/{sample}.bam.bai",
        targets=config["ampliconBED"]
    params:
        minCov = 20,
        minMAF = 0.2
    threads: 2
    output:
        "variants/{sample}_varscan.vcf"
    shell:
        "samtools mpileup -B -f {input.ref} -l {input.targets} {input.bam} | java -Xmx2G -jar ~/tools/VarScan.v2.4.3.jar mpileup2snp --output-vcf 1 --variants --min-coverage {params.minCov} --min-var-freq {params.minMAF} > {output}"

rule call_consensus_varscan:
    input:
        ref = config["ref_genome"],
        bam = "bam_mapq/{sample}.bam",
        bai = "bam_mapq/{sample}.bam.bai",
        targets=config["ampliconBED"]
    params:
        minCov = 100,
        minMAF = 0.2
    threads: 2
    output:
        "consensus/{sample}_varscan.vcf"
    shell:
        "samtools mpileup -B -f {input.ref} -l {input.targets} {input.bam} | java -Xmx4G -jar ~/tools/VarScan.v2.4.3.jar mpileup2cns --output-vcf 1 --min-coverage {params.minCov} --min-var-freq {params.minMAF} > {output}"

rule call_consensus_varscan_WES:
    input:
        ref = config["ref_genome"],
        bam = "/lnec/SANSON/nanopore/WES/{sample}.bam",
        bai = "/lnec/SANSON/nanopore/WES/{sample}.bam.bai",
        targets=config["ampliconBED"]
    params:
        minCov = 10,
        minMAF = 0.2
    threads: 2
    output:
        "consensus_WES/{sample}_varscan.vcf"
    shell:
        "samtools mpileup -B -f {input.ref} -l {input.targets} {input.bam} | java -Xmx4G -jar ~/tools/VarScan.v2.4.3.jar mpileup2cns --output-vcf 1 --min-coverage {params.minCov} --min-var-freq {params.minMAF} > {output}"

rule call_consensus_varscan_BrainCap:
    input:
        ref = config["ref_genome"],
        bam = "/lnec/SANSON/nanopore/BrainCap/{sample}.bam",
        bai = "/lnec/SANSON/nanopore/BrainCap/{sample}.bam.bai",
        targets=config["ampliconBED"]
    params:
        minCov = 100,
        minMAF = 0.2
    threads: 2
    output:
        "consensus_BrainCap/{sample}_varscan.vcf"
    shell:
        "samtools mpileup -B -f {input.ref} -l {input.targets} {input.bam} | java -Xmx4G -jar ~/tools/VarScan.v2.4.3.jar mpileup2cns --output-vcf 1 --min-coverage {params.minCov} --min-var-freq {params.minMAF} > {output}"

rule BEDtools_base_coverage:
    input:
        bam = "bam_mapq/{sample}.bam",
        bai = "bam_mapq/{sample}.bam.bai",
        ampliconBED = config["ampliconBED"]
    output:
        "coverage/{sample}.txt"
    shell:
        "bedtools coverage -b {input.bam} -a {input.ampliconBED} -d > {output}"

rule coverage_plots:
    input:
        "coverage/{sample}.txt"
    output:
        sumPDF="plots/{sample}_cov_summary.pdf",
        detailPDF="plots/{sample}_cov_detail.pdf"
    script:
        "scripts/makeAmpliconCoveragePlot.R"

rule prefilter_variants:
    input:
        vcf="variants/{sample}.vcf",
        ref = config["ref_genome"]
    output:
        "variants_prefiltered/{sample}.vcf"
    shell:
        "export JAVA_HOME=/export/data/applications/java/jre1.8.0_25 ; "
        "export PATH=$JAVA_HOME/bin:$PATH ; "
        "export LD_LIBRARY_PATH=\"$JAVA_HOME/lib/amd64:$JAVA_HOME/lib/amd64/server\" ;"
        "java -Xmx4g -jar ~/tools/GenomeAnalysisTK-3.7.jar"
        "   -T VariantAnnotator"
        "   -R {input.ref} -U ALLOW_SEQ_DICT_INCOMPATIBILITY"
        "   -V {input.vcf}"
        "   -L {input.vcf}"
        "   -o {output}"
        "   -A StrandOddsRatio"

rule annotate_variants:
    input:
        vcf="variants/{sample}.vcf",
        exac=config["ExAC"],
        hotspots=config["CancerHotspots"]
    output:
        "variants_annotated/{sample}.vcf"
    shell:
        "java -Xmx2G -jar ~/tools/snpEff/snpEff.jar hg19kg {input.vcf} | "
        "java -Xmx2G -jar ~/tools/snpEff/SnpSift.jar annotate -exists ExAC {input.exac} /dev/stdin | "
        "java -Xmx2G -jar ~/tools/snpEff/SnpSift.jar annotate -exists CancerHotspot {input.hotspots} /dev/stdin > {output}"

rule filter_variants:
    input:
        vcf="variants_annotated/{sample}.vcf",
        targets=config["ampliconBED"]
    output:
        "variants_filtered/{sample}.vcf"
    params:
        filter="(CHROM='chr5' & (POS=1295228 | POS=1295250)) | (!( ID =~ 'rs' ) & ((ANN[*].IMPACT has 'MODERATE') | (ANN[*].IMPACT has 'HIGH')))"
    shell:
        "java -Xmx2G -jar ~/tools/snpEff/SnpSift.jar filter \"{params.filter}\" {input.vcf} | "
        "java -Xmx2G -jar ~/tools/snpEff/SnpSift.jar intervals {input.targets} > {output}"

rule calc_condordance_WES:
    input:
        nanopore="consensus/{sample}-amplicon_varscan.vcf",
        truth="consensus_WES/{sample}_varscan.vcf",
        ref = "/lnec/SANSON/ABM/BrainCapPipeline/ref/ucsc.hg19.fasta"
#        ref = config["ref_genome"]
    output:
        "reports/{sample}_concordance.txt"
    shell:
        "export JAVA_HOME=/export/data/applications/java/jre1.8.0_25 ; "
        "export PATH=$JAVA_HOME/bin:$PATH ; "
        "export LD_LIBRARY_PATH=\"$JAVA_HOME/lib/amd64:$JAVA_HOME/lib/amd64/server\" ;"
        "java -Xmx4g -jar ~/tools/GenomeAnalysisTK-3.7.jar"
        " -T GenotypeConcordance"
        " -R {input.ref} -U ALLOW_SEQ_DICT_INCOMPATIBILITY"
        " -eval {input.nanopore}"
        " -comp {input.truth}"
        " -o {output}"

rule calc_condordance_BrainCap:
    input:
        nanopore="consensus/{sample}-amplicon_varscan.vcf",
        truth="consensus_BrainCap/{sample}_varscan.vcf",
        ref = "/lnec/SANSON/ABM/BrainCapPipeline/ref/ucsc.hg19.fasta"
#        ref = config["ref_genome"]
    output:
        "reports1/{sample}_concordance.txt"
    shell:
        "export JAVA_HOME=/export/data/applications/java/jre1.8.0_25 ; "
        "export PATH=$JAVA_HOME/bin:$PATH ; "
        "export LD_LIBRARY_PATH=\"$JAVA_HOME/lib/amd64:$JAVA_HOME/lib/amd64/server\" ;"
        "java -Xmx4g -jar ~/tools/GenomeAnalysisTK-3.7.jar"
        " -T GenotypeConcordance"
        " -R {input.ref} -U ALLOW_SEQ_DICT_INCOMPATIBILITY"
        " -eval {input.nanopore}"
        " -comp {input.truth}"
        " -o {output}"



localrules: report_variants
rule report_variants:
    input:
        expand("variants_filtered/{sample}_varscan.vcf",sample=SAMPLES)
    output:
        "reports/variants_filtered.xlsx"
    script:
        "scripts/reportMutations.R"


rule calcAmpliconRuntime:
    input:
        config["FAST5_basedir"] + "{sample}/"
    output:
        "stats/{sample}.times.RData"
    wildcard_constraints:
        sample="|".join(NONBARCODED)
    script:
        "scripts/calcRuntime.R"

rule calcAmpliconRuntimeBarcoded:
    input:
        lambda wildcards : "demux/" + DEMUX[wildcards.sample] + "/workspace/" + BARCODES[wildcards.sample] + "/"
    output:
        "stats/{sample}.times.RData"
    wildcard_constraints:
        sample="|".join(DEMUX.keys())
    script:
        "scripts/calcRuntime.R"


rule ampliconRuntimeTable:
    input:
        expand("stats/{sample}.times.RData", sample=SAMPLES)
    output:
        "reports/ampliconRuntimes.txt"
    script:
        "scripts/makeRuntimeTable.R"

rule plotReadsPerTargetOverTime:
    input:
        times="stats/{sample}.times.RData",
        bam="bam_mapq/{sample}.bam",
        fa="fasta/{sample}.fa",
        targets=config["ampliconBED"]
    output:
        bed=temp("temp/{sample}_ReadsPerTarget.bed"),
        pdf="plots/{sample}_reads_over_time.pdf"
    script:
        "scripts/plotReadsPerAmpliconOverTime.R"

#### $tools/bedtools-2.26.0/bin/bedtools intersect -a /lnec/SANSON/nanopore/amplicon/bam/2483T-amplicon_pass.bam -b /lnec/SANSON/nanopore/pipeline/data/nanopanel_v3_target_regions.bed -bed -wb > test.bed


# build OpenBLAS
# make USE_OPENMP=1
# ln -s libopenblas_sandybridgep-r0.2.19.a libblas.a

# export OMP_NUM_THREADS=`nproc`
# find /lnec/SANSON/nanopore/runs/2965T-amplicon/pass/downloads/pass -name \*.fast5 | xargs ~/tools/scrappie/build/scrappie > 2965T-amplicon-scrappie.fa
